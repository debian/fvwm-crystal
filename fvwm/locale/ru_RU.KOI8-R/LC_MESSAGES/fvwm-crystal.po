# Russian translation for FVWM-Crystal
# Copyright (C) 2008 FVWM-Crystal team
# This file is distributed under the same license as the fvwm-crystal package.
# Maciej Delmanowski <harnir@post.pl>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: fvwm-crystal\n"
"POT-Creation-Date: 2005-07-04 14:23+0100\n"
"PO-Revision-Date: 2008-11-30 14:00+0300\n"
"Last-Translator: Alexander Galanin <gaa.nnov@mail.ru>\n"
"Language-Team: Russian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=koi8-r\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "About FVWM-Crystal"
msgstr "O FVWM-Crystal"

msgid "Activate"
msgstr "������� ��������"

#
# Applications menu
#
msgid "Applications"
msgstr "���������"

msgid "Button model"
msgstr "������ ������"

msgid "Cancel"
msgstr "��������"

msgid "Clear playlist"
msgstr "�������� ��������"

msgid "Close"
msgstr "�������"

msgid "Colorsets"
msgstr "������ ������"

msgid "Crossfade"
msgstr "��������� ������������"

msgid "Default terminal"
msgstr "����������� ��������"

msgid "Delayed fullscreen"
msgstr "���������� ������ �����"

msgid "Desktop manager"
msgstr "�������� ��������"

msgid "Destroy"
msgstr "����������"

msgid "Developer menu"
msgstr "���� ������������"

msgid "Everything"
msgstr "�ӣ"

msgid "Exit"
msgstr "�����"

#
# Screensaver menu
#
msgid "Force suspend"
msgstr "��������� �����������"

msgid "Frame"
msgstr "�����"

#
# Screenshot menu
#
msgid "Fullscreen"
msgstr "������ �����"

msgid "I am sure"
msgstr "� ������"

#
# Window menus
#
msgid "Iconify"
msgstr "��������"

msgid "Identify"
msgstr "����������������"

msgid "Jump to left page"
msgstr "������������� �� ����� ��������"

msgid "Jump to lower page"
msgstr "������������� �� �������� ����"

msgid "Jump to previous page"
msgstr "������������� �� ���������� ��������"

msgid "Jump to right page"
msgstr "������������� �� ������ ��������"

msgid "Jump to upper page"
msgstr "������������� �� �������� ����"

msgid "Load playlist"
msgstr "��������� ��������"

msgid "Lock screen"
msgstr "������������� �����"

#
# Exit menu
#
msgid "Logout"
msgstr "��������� �����"

msgid "Maximize"
msgstr "���������"

msgid "Maximize horizontally"
msgstr "��������� �� �����������"

msgid "Maximize vertically"
msgstr "��������� �� ���������"

msgid "Move to left page"
msgstr "����������� �����"

msgid "Move to lower page"
msgstr "����������� �� �������� ����"

msgid "Move to page"
msgstr "����������� �� ��������"

msgid "Move to previous page"
msgstr "����������� �� ���������� ��������"

msgid "Move to right page"
msgstr "����������� ������"

msgid "Move to upper page"
msgstr "����������� �� �������� ����"

#
# Music menu
#
msgid "Music"
msgstr "������"

msgid "Music player"
msgstr "������������������"

msgid "Name of the playlist"
msgstr "�������� ���������"

msgid "New recipe will be used after next startup"
msgstr "����� ����� ����� ������������ ��� ��������� �������"

msgid "None"
msgstr "�����"

msgid "Normal mode"
msgstr "������� ���"

msgid "Not now"
msgstr "�� ������"

msgid "Off"
msgstr "���������"

msgid "On"
msgstr "��������"

msgid "Play/Pause"
msgstr "���������������/�����"

msgid "Player is stopped"
msgstr "������������� ����������"

msgid "Playlist editor"
msgstr "�������� ���������"

msgid "Preferences"
msgstr "���������"

msgid "Privileged terminals"
msgstr "����������������� ���������"

msgid "QuakeConsole terminal"
msgstr "�������� ��� QuakeConsole"

#
# Wallpaper menu
#
msgid "Random"
msgstr "��������� �������"

msgid "Reboot computer"
msgstr "������������� ���������"

msgid "Refresh"
msgstr "��������"

msgid "Remove current song"
msgstr "������� ������� ������������"

msgid "Remove playlist"
msgstr "������� ��������"

msgid "Restart"
msgstr "�������������"

msgid "Save"
msgstr "���������"

msgid "Save playlist"
msgstr "��������� ��������"

msgid "Save playlist..."
msgstr "��������� ��������"

msgid "Screensaver"
msgstr "��������"

msgid "Screenshot"
msgstr "��������"

msgid "Selected desktop manager will be used"
msgstr "������� ������������ �������� ��������"

msgid "Show/hide player"
msgstr "��������/������ �����"

msgid "Shuffle playlist"
msgstr "������ � ��������� �������"

msgid "Start screensaver"
msgstr "������ ������������"

msgid "Stay lowered"
msgstr "������ �����"

msgid "Stay raised"
msgstr "������ �������"

msgid "Stick"
msgstr "���������"

msgid "Stop playback"
msgstr "����"

msgid "Stop screensaver"
msgstr "����� ������������"

#
# String used in submenus (window decorations and colorsets)
#
msgid "System"
msgstr "��������� ����"

msgid "Take all free space"
msgstr "������ �ӣ ��������� ������������"

msgid "Take all horizontal space"
msgstr "������ �ӣ �������������� ������������"

msgid "Take all vertical space"
msgstr "������ �ӣ ������������ ������������"

msgid "Toggle random"
msgstr "�������� ��������� ������� ���������������"

msgid "Toggle repeat"
msgstr "�������� ������"

msgid "Turn off computer"
msgstr "��������� ���������"

msgid "Update database"
msgstr "�������� ��"

#
# Preferences menu
#
msgid "Used recipe"
msgstr "������������ �����"

msgid "User"
msgstr "���������������� ����"

msgid "Volume"
msgstr "���������"

msgid "Wallpaper"
msgstr "����"

#
# Main menu - usually found in recipes/*
#
msgid "Window decorations"
msgstr "��������� ����"

msgid "Window focus policy"
msgstr "�������� ������"

msgid "at the next start of your X session"
msgstr "��� ��������� ������ �-������"
