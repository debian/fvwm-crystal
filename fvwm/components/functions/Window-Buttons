# Menus for window buttons
# Usage:
#
# Mouse (mbutton#) (button#) (mod) Window-Button-Function (Button#) (menu) (function)
#
# Where:
# mbutton# - number of the mouse button (0 - all buttons, 1 - left button,
# 2 - middle button, 3 - right button)
# button# - number of the window title button, 0-9
# mod - modifier, see ~/.fvwm/functions/Keyboard-Modifiers
# Button# - number of the window title button, Button0-Button9
# menu - name of the menu opened on the mouse hold
# function - function executed after mouse click

# The windows can have several states: its original or mouse resized state,
# a minimized state, a maximized state, a fullscreen state, and several other.
# A mouse click must set the windows into the wanted state, and when in
# fullscreen, the window must retrun in its precedent state.
# We need 3 environment variables per windows, 2 will contain its original or
# mouse resized width and height, the third one will contain a string
# corresponding to its state (excepted fullscreen).
# The fullscreen functions are implemented into the Fullscreen file.

# Variables creation and destruction {{{1
# They must survive a restart => SetEnv
# FvwmEvent will do it.
DestroyFunc Window-State-Init
AddToFunc Window-State-Init
+ I SetEnv "CurrentWindowState_$[w.id]" Default
+ I SetEnv "WindowWidth_$[w.id]"  "$[w.width]p"
+ I SetEnv "WindowHeight_$[w.id]" "$[w.height]p"

DestroyFunc Window-State-Destroy
AddToFunc Window-State-Destroy
+ I UnsetEnv "CurrentWindowState_$[w.id]"
+ I UnsetEnv "WindowWidth_$[w.id]"
+ I UnsetEnv "WindowHeight_$[w.id]"

# Variables update, see also components/Window-Basic

# param for fvwm-event: add_window, destroy_window, property_change
DestroyModuleConfig FvwmEvent-Window-State-Init: *
*FvwmEvent-Window-State-Init: add_window Window-State-Init
*FvwmEvent-Window-State-Init: destroy_window Window-State-Destroy
Module FvwmEvent FvwmEvent-Window-State-Init

## Wrapper function for the resizing functions
# Logic: if actual_size=size then default_size else size
# Syntax: Window-Resize <size>
# Where size is one of Maximize-Minimize-Default-A100-H100-V100-VHgrow-Hgrow-Vgrow
# For fullscreen see components/Fullscreen
DestroyFunc Window-Resize
AddToFunc Window-Resize
+ I Test (EnvMatch CurrentWindowState_$[w.id] "$0") NS-Default
+ I TestRc (NoMatch) NS-"$0"

DestroyFunc NS-Maximize
AddToFunc NS-Maximize
+ I Maximize True 100 100
+ I SetEnv CurrentWindowState_$[w.id] Maximize

DestroyFunc NS-Minimize
AddToFunc NS-Minimize
+ I ResizeMaximize direction East 200p 60p
+ I SetEnv CurrentWindowState_$[w.id] Minimize

DestroyFunc NS-Default
AddToFunc NS-Default
+ I Maximize $[WindowWidth_$[w.id]] $[WindowHeight_$[w.id]]
+ I SetEnv CurrentWindowState_$[w.id] Default

DestroyFunc NS-A100
AddToFunc NS-A100
+ I Maximize True 100 100
+ I SetEnv CurrentWindowState_$[w.id] A100

DestroyFunc NS-H100
AddToFunc NS-H100
+ I Maximize True 100 0
+ I SetEnv CurrentWindowState_$[w.id] H100

DestroyFunc NS-V100
AddToFunc NS-V100
+ I Maximize True 0 100
+ I SetEnv CurrentWindowState_$[w.id] V100

DestroyFunc NS-Hgrow
AddToFunc NS-Hgrow
+ I Maximize True grow 0
+ I SetEnv CurrentWindowState_$[w.id] Hgrow

DestroyFunc NS-Vgrow
AddToFunc NS-Vgrow
+ I Maximize True 0 grow
+ I SetEnv CurrentWindowState_$[w.id] Vgrow

DestroyFunc NS-VHgrow
AddToFunc NS-VHgrow
+ I Maximize True grow grow
+ I SetEnv CurrentWindowState_$[w.id] VHgrow

# NS-Default rearrange
DestroyFunc NS-Default-Rearrange
AddToFunc NS-Default-Rearrange
+ I All (CurrentPage, !Iconic, !FvwmButtons, !FvwmPager, !FvwmConsoleNeedsUniqueName, !QuakeConsoleNeedsUniqueName, !FvwmIconMan, !MPlayer) NS-Default
+ I All (CurrentPage, !Iconic, !FvwmButtons, !FvwmPager, !FvwmConsoleNeedsUniqueName, !QuakeConsoleNeedsUniqueName, !FvwmIconMan, !MPlayer) PlaceAgain Anim

# Wrapper functions for window decorations {{{1
DestroyFunc Window-MoveToPage-Forward
AddToFunc Window-MoveToPage-Forward
+ I MoveToPage-Right

DestroyFunc Window-MoveToPage-Backward
AddToFunc Window-MoveToPage-Backward
+ I MoveToPage-Left

DestroyFunc Window-WarpToPage-Forward
AddToFunc Window-WarpToPage-Forward
+ I WarpToPage-Right

DestroyFunc Window-WarpToPage-Backward
AddToFunc Window-WarpToPage-Backward
+ I WarpToPage-Left

DestroyFunc Window-Button-Function
AddToFunc Window-Button-Function
+ H Popup $1 $0 0 100
+ C $2 $3 $4 $5 $6 $7 $8 $9

DestroyMenu /Window-Menu-Close
AddToMenu /Window-Menu-Close
+ '$[gt.Iconify]'	Window-Iconify
+ '$[gt.Close]'		Close
+ '$[gt.Destroy]'	Destroy
+ "" Nop
+ '$[gt.Identify]'	FvwmIdent

DestroyMenu /Window-Menu-Maximize
AddToMenu /Window-Menu-Maximize
+ '$[gt.Maximize]'			Window-Resize Maximize
+ '$[gt.Minimize]'			Window-Resize Minimize
+ '$[gt.Maximize horizontally]'		Window-Resize H100
+ '$[gt.Maximize vertically]'		Window-Resize V100
+ "" Nop
+ '$[gt.Take all free space]'		Window-Resize VHgrow
+ '$[gt.Take all horizontal space]'	Window-Resize Hgrow
+ '$[gt.Take all vertical space]'	Window-Resize Vgrow
+ "" Nop
+ '$[gt.Fullscreen]'			Fullscreen

DestroyMenu /Window-Menu-Layer
AddToMenu /Window-Menu-Layer
+ '$[gt.Stay raised]'	Layer 0 5
+ '$[gt.Normal mode]'	Layer 0 4
+ '$[gt.Stay lowered]'	Layer 0 3
+ '$[gt.Stick]'		Stick
+ "" Nop
+ '$[gt.Refresh]'	RefreshWindow

DestroyMenu /Window-Menu-Page
AddToMenu /Window-Menu-Page
+ DynamicPopupAction Function /Window-Menu-Page-generator

DestroyFunc /Window-Menu-Page-generator
AddToFunc /Window-Menu-Page-generator
+ I DestroyMenu recreate /Window-Menu-Page
+ I AddToMenu /Window-Menu-Page '$[gt.Move to left page]'		Window-MoveToPage-Backward
+ I AddToMenu /Window-Menu-Page '$[gt.Move to previous page]'		MoveToPage-Focus prev
+ I AddToMenu /Window-Menu-Page '$[gt.Move to right page]'		Window-MoveToPage-Forward
+ I AddTomenu /Window-Menu-Page "" Nop
+ I AddToMenu /Window-Menu-Page '$[gt.Jump to left page]'		Window-WarpToPage-Backward
+ I AddToMenu /Window-Menu-Page '$[gt.Jump to previous page]'		WarpToPage prev
+ I AddToMenu /Window-Menu-Page '$[gt.Jump to right page]'		Window-WarpToPage-Forward
+ I AddToMenu /Window-Menu-Page "" Nop
+ I PipeRead 'for i in `seq 0 $(($[desk.pagesx]-1))` ; do echo "AddToMenu /Window-Menu-Page \'$[gt.Move to page] $(($i+1))\' MoveToPage $i 0" ; done'


# Combined version of menu for window styles other than FVWM-Crystal and Amiga {{{1
DestroyMenu /Window-Menu
AddToMenu /Window-Menu
+ '$[gt.Iconify]'		Window-Iconify
+ '$[gt.Stick]'			Stick
+ '$[gt.Stay raised]'		Layer 0 5
+ '$[gt.Normal mode]'		Layer 0 4
+ '$[gt.Maximize]'		Maximize
+ '$[gt.Fullscreen]'		Fullscreen
+ "" Nop
+ '$[gt.Move to left page]'	Window-MoveToPage-Backward
+ '$[gt.Move to right page]'	Window-MoveToPage-Forward
+ "" Nop
+ '$[gt.Identify]'		FvwmIdent
+ '$[gt.Close]'			Close

# vim:ft=fvwm
